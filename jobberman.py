

!pip install selenium
!pip install webdriver_manager
!apt-get update # to update ubuntu to correctly run apt install
!apt install chromium-chromedriver
!cp /usr/lib/chromium-browser/chromedriver /usr/bin
import sys
sys.path.insert(0,'/usr/lib/chromium-browser/chromedriver')
from selenium import webdriver
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome('chromedriver',chrome_options=chrome_options)

import csv
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException,NoSuchElementException
import time
from webdriver_manager.chrome import ChromeDriverManager
from time import sleep
import re
import pandas as pd
pd.set_option('display.max_rows', None)

def strip_unwanted_text(s,start,end):
    return s[s.find(start)+len(start):s.rfind(end)]

#Creating the dataset
job_dataset=pd.DataFrame(columns=['company_name','job_title','job_type','location','job_desription','skill_set_required',
                                  'salary','deadline'
                                  # ,'date_published','offer_type',
                                  # 'years_of_experience_required','job_location',
                                  # 'job_specialisation','job_industry','position'
                                  
                                 ])






url='https://www.jobberman.com/jobs'
job=[]
job_link=[]

driver.get('https://www.jobberman.com/jobs')
soup = BeautifulSoup(driver.page_source, 'html.parser')
job=[i for i in soup.find_all('article',attrs={"class" : "search-result "})]
for c in job:
  job_link.append(c.find('a', href=True)['href']) #get job url

job_link

full_job_url="https://www.jobberman.com/listings/graphic-designer-7509q8"
driver.get(full_job_url)
print(full_job_url)
soup = BeautifulSoup(driver.page_source, 'html.parser')

soup.find('div',attrs={"class" : "search-result__job-salary"}).text.strip()

# for link in job_link:
#   full_job_url=link
#   driver.get(full_job_url)
#   print(full_job_url)
#   soup = BeautifulSoup(driver.page_source, 'html.parser')
#   company_details=soup.find('h1',attrs={"class" : "job-header__title"})
#   # company_name  = soup.find('a',attrs={"class" : "color--default-text"}).text.strip()
#   position=company_details
#   job_title  = position
#   job_descr=soup.find('div',attrs={"class" : "job__details__user-edit description-content__content"})

#   s1 = soup.find('div',attrs={"class" : "job__details__user-edit description-content__content"})
#   skill_set_required   = strip_unwanted_text(s1.text,'Requirements:','') 
#   job_summary=soup.find('ul',attrs={"class" : "wrapper--inline-block float--left margin-top--15 padding-left--20 font--weight-300"})
#   years_of_experience_needed=strip_unwanted_text(job_summary.text,'Experience Length:','')        
#   minimum_job_qualification=strip_unwanted_text(job_summary.text,'Minimum Qualification:','Experience Level:')
#   company_name  = soup.find('div',attrs={"class" : "if-content-panel padding-lr-20 flex-direction-top-to-bottom--under-lg align--start--under-lg search-result__job-meta"}).text.strip()
#   date_published = soup.find('span',attrs={"class" : "job-header__time-stamp line--height-15"}).text.strip()
#   offer_type = soup.find('span',attrs={"class" : "job-header__industry"}).text.strip()
#   job_location = soup.find('span',attrs={"class" : "job-header__location margin-top--10-under-lg "}).text.strip()
#   deadline  = None
#   job_specialisation = strip_unwanted_text(soup.find('div',attrs={"class" : "job__details__user-edit description-content__content"}).text,"Other Requirements","Skills & Experience:")
#   job_description=job_descr
#   salary  = soup.find('div',attrs={"class" : "search-result__job-salary"}).text.strip()
#   job_industrywrklevel  = soup.find('span',attrs={"class" : "job-header__work-type"}).text.strip()
    
  




#   if company_name is None:
#     company_name='N/A'
#   else:
#     company_name=company_name
#   if position is None:
#     position='N/A'
#   else:
#     position=position.text
#   if job_descr is None:
#     job_description='N/A'
#   else:
#     job_description=job_descr.text
#   print(job_summary.text)
#   print(company_name)
#   print(position)
#   print(date_published)
#   print(offer_type)
#   print(years_of_experience_needed)
#   print(job_location)
#   print(deadline)
#   print(job_specialisation)
#   print(job_industrywrklevel)
#   print(job_description)
#   print(salary)
                
#   job_dataset = job_dataset.append(pd.DataFrame({'company_name':str(company_name),
#                                               'position':str(position),
#                                                 'job_title':str(job_title),
#                                                   'job_type':str(job_type),
#                                                   'location':str(job_location),
#                                                     'job_desription':str(job_description),
#                                                      'skill_set_required':str(skill_set_required),
#                                                      'salary':str(salary),
#                                                       'deadline':str(deadline),
#                                               'date_published':str(date_published),
#                                               'offer_type':str(offer_type),
#                                               'years_of_experience_required':str(years_of_experience_needed),
#                                               'job_location':str(job_location),
#                                               'deadline':str(deadline),
#                                               'job_specialisation':str(job_specialisation),
#                                               'job_industrywrklevel':str(job_industrywrklevel)}, index=[0]), ignore_index=True)



next_job='https://www.jobberman.com/jobs/index/'
for i in range(2,100):
    pagination_url=next_job+str(i)
    driver.get(pagination_url)
    sleep(1)
        
    job_link=[]
    job=[]
    soup = BeautifulSoup(driver.page_source, 'html.parser')


    job=[i for i in soup.find_all('article',attrs={"class" : "search-result "})]
    for c in job:
      job_link.append(c.find('a', href=True)['href']) #get job url


    for link in job_link:
      full_job_url=link
      driver.get(full_job_url)
      print(full_job_url)
      soup = BeautifulSoup(driver.page_source, 'html.parser')
      company_details=soup.find('h1',attrs={"class" : "job-header__title"})
      # company_name  = soup.find('a',attrs={"class" : "color--default-text"}).text.strip()
      position=company_details
      job_title  = position.text
      job_descr=soup.find('div',attrs={"class" : "job__details__user-edit description-content__content"})

      # job_summary=soup.find('ul',attrs={"class" : "wrapper--inline-block float--left margin-top--15 padding-left--20 font--weight-300"})
      # years_of_experience_needed=strip_unwanted_text(job_summary.text,'Experience Length:','')        
      # minimum_job_qualification=strip_unwanted_text(job_summary.text,'Minimum Qualification:','Experience Level:')
      company_name  = soup.find('div',attrs={"class" : "if-content-panel padding-lr-20 flex-direction-top-to-bottom--under-lg align--start--under-lg search-result__job-meta"}).text.strip()
      # date_published = soup.find('span',attrs={"class" : "job-header__time-stamp line--height-15"}).text.strip()
      offer_type = soup.find('span',attrs={"class" : "job-header__industry"})
      job_type   = offer_type
      job_location = soup.find('span',attrs={"class" : "job-header__location margin-top--10-under-lg "}).text.strip()
      deadline  = None
      # job_specialisation = strip_unwanted_text(soup.find('div',attrs={"class" : "job__details__user-edit description-content__content"}).text,"Other Requirements","Skills & Experience:")
      
      job_specialisation =soup.find('div',attrs={"class" : "job-header__footer"}).find_all("a",href =True)[1].text.strip()

      job_description=job_descr
      salary  = soup.find('div',attrs={"class" : "search-result__job-salary"}).text.strip()
      # job_industrywrklevel  = soup.find('span',attrs={"class" : "job-header__work-type"}).text.strip()

      s1 = soup.find('div',attrs={"class" : "job__details__user-edit description-content__content"})
      skill_set_required   = strip_unwanted_text(s1.text,'Requirements:','') 
      location  = job_location
      if company_name is None:
        company_name='N/A'
      else:
        company_name=company_name
      if position is None:
        position='N/A'
      else:
        position=position.text
      if job_descr is None:
        job_description='N/A'
      else:
        job_description=job_descr.text

# 'company_name','job_title','job_type','location','job_desription','skill_set_required',
#                                   'salary','deadline'
      # print(job_summary.text)
      print(company_name)
      print(position)
      # print(date_published)
      print(offer_type)
      # print(years_of_experience_needed)
      print(job_location)
      print(deadline)
      # print(job_specialisation)
      # print(job_industrywrklevel)
      print(job_description)
      print(salary)
            
            
        
              
      job_dataset = job_dataset.append(pd.DataFrame({'company_name':str(company_name),
                                              'job_title':str(position),
                                                'job_title':str(job_title),
                                                  'job_type':str(job_type),
                                                  'location':str(job_location),
                                                    'job_desription':str(job_description),
                                                     'skill_set_required':str(skill_set_required),
                                                     'salary':str(salary),
                                                      'deadline':str(deadline),
                                              # 'date_published':str(date_published),
                                              # 'offer_type':str(offer_type),
                                              # 'years_of_experience_required':str(years_of_experience_needed),
                                              # 'job_location':str(job_location),
                                              # 'deadline':str(deadline),
                                              'job_specialisation':str(job_specialisation),
                                              # 'job_industrywrklevel':str(job_industrywrklevel)
                                              }, index=[0]), ignore_index=True)

job_dataset.head()

job_dataset

job_dataset.to_csv('jobberman.csv',index = False)

from google.colab import files

files.download("jobberman.csv")

